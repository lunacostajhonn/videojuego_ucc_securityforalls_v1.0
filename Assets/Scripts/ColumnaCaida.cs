﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnaCaida : MonoBehaviour
{

    // Todas las variables de objeto que declare públicas, se van a visualizar en el inspector
    // y pueden recibir modificaciones.
    public GameObject columna;
    Animator colum_anim;
    // Start is called before the first frame update
    void Start()
    {   // Para obtener acceso a los componentes que son objetos de un GameObject, utilizo el médtodo
        // GetComponent<tipoobjeto>()

        colum_anim = columna.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// OnTriggerEnter is called  when the Collider enters the
    /// </summary>
    /// <param name = "other">The other Collider involved  in this Collider
    void OnTriggerEnter(Collider personaje)
    {
        if (personaje.tag == "Player")
        {
            colum_anim.SetTrigger("caerColumna");
        }
    }
}
