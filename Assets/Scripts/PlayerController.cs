﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Referencia al controlador de animaciones.

    private Animator animator;
    // Determinamos si está avanzando o está quieto
    private bool avanzando;

    // Variables relacionadas con el movimiento del personaje
    public float velocidad = 3.0f;
    // Para las funciones relacionadas con físicas debo garantizar el acceso al RigidBody del personaje
    private Rigidbody rb;
    // Definición de un vector de dirección de movimiento.
    Vector3 direccion;

    // Nivel de vida
    public int vida = 100;


    // Start is called before the first frame update
    void Start()
    {
        // Es recomendable en éste método configurar la inicialización  de nuestras variables de objetos de juego.

        // Accedemos al RigidBody
        rb = GetComponent<Rigidbody>();
        // Acceder al componente de animaciones "ejecute x animación"
        animator = GetComponent<Animator>();

    }

    public void QuitarVida(int perdidaVida)
    {
        vida = vida - perdidaVida;
        Debug.Log("nivel de vida >>> " + vida);
    }

    // Update is called once per frame
    void Update()
    {
        // Se ejecuta en éste método cada Frame sin validaciones
        // No se recomienda programar comportamientos físicos.

    }

    void FixedUpdate()
    {
        // Se ejecuta en cada Frame pero con validación, valida su ejecución.

        // Para la animación de escribir en el PC.
        if (Input.GetKeyDown(KeyCode.G))
        {

            animator.SetTrigger("Escribiendo_pc");

        }
        // Para la animación de sentarse
        if (Input.GetButton("Fire1"))
        {

            animator.SetTrigger("Sentarse");

        }
        // Para la animación de morir
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            animator.SetTrigger("Morir");
        }

        // Para correr
        if (Input.GetKeyDown("space"))
        {
            animator.SetTrigger("Correr");
        }

        // En guardia
        //if(Input.GetKeyDown(KeyCode.E)){
        //    animator.SetTrigger("Recibir_Golpe")
        // }





        // Identifico las variables de entrada de usuario.
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        // En cada frame valido el movimiento del personaje.
        Movimiento(horizontal, vertical);
        // Ejecuto la animación correspondiente al movimientio---> Caminar
        Animacion(horizontal, vertical);
    }

    public void Animacion(float horizontal, float vertical)
    {
        Debug.Log("horizontal >>> " + horizontal + " / vertical >>> " + vertical);
        avanzando = horizontal != 0f || vertical != 0f;
        Debug.Log("avanzando >>> " + avanzando);
        animator.SetBool("Caminar", avanzando);

    }

    //public void QuitarVida(int perdidaVida){
    //vida = vida - perdidaVida;
    //Debug.Log("nivel de vida >>>" + vida);
    //} 

    public void Movimiento(float horizontal, float vertical)
    {
        // Se define un vector relacionado con la dirección hacia la cual se mueve el personaje.
        direccion.Set(horizontal, 0f, vertical);
        direccion = direccion.normalized * velocidad * Time.deltaTime;
        // Validar el RigidBody asignado al personaje
        if (rb)
        {
            if (horizontal > 0.0f)
            {
                rb.transform.Translate(
                    Vector3.right * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (horizontal < 0.0f)
            {
                rb.transform.Translate(
                    Vector3.left * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (vertical > 0.0f)
            {
                rb.transform.Translate(
                    Vector3.forward * velocidad * Time.deltaTime
                );
            }
            if (vertical < 0.0f)
            {
                rb.transform.Translate(
                    Vector3.back * velocidad * 0.5f * Time.deltaTime
                );
            }
        }
    }
}
