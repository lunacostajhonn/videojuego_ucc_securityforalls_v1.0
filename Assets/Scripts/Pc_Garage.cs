﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pc_Garage : MonoBehaviour
{

    public Text mensaje;
    public GameObject bloqueador;

    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            StartCoroutine("AprendiendoSeguridad");
        }
    }

    public IEnumerator AprendiendoSeguridad()
    {
        mensaje.text = "Descargando información";
        yield return new WaitForSeconds(2.0f);
        mensaje.text = "";
        Destroy(bloqueador, 1.0f);
    }

}
