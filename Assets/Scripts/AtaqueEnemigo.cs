﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueEnemigo : MonoBehaviour
{
    public PlayerController player;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            player.QuitarVida(10);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
