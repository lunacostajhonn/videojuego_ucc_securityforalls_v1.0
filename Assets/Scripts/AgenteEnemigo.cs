﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgenteEnemigo : MonoBehaviour{

    public Transform posicionPersonaje;
    public float distanciaDeteccion = 10.0f;
    public NavMeshAgent agente;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(this.transform.position, posicionPersonaje.transform.position) < distanciaDeteccion){
             agente.SetDestination(posicionPersonaje.transform.position);

        }
       
        
    }

    
}
