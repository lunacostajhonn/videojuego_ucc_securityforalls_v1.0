﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salir : MonoBehaviour
{
    public void Salirdeljuego()
    {
        // Mensaje del juego
        Debug.Log("Saliendo del VideoJuego");

        // Método para salir del juego
        Application.Quit();
    }
}
