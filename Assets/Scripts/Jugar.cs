﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Jugar : MonoBehaviour
{
    // Se declara un método para ser invocado dentro del script

    public void IniciarJuego()
    {

        // Utilizo un método para cargar la scena de vivel1
        SceneManager.LoadScene("SampleScene");

    }

}

